"use strict";

// assign button elements in the array
let buttons = [
  document.getElementById("button0"),
  document.getElementById("button1"),
  document.getElementById("button2"),
  document.getElementById("button3")
];

// button colors are stored inside an array
const colors = ["Blue", "DarkOrange", "Green", "Yellow"];

init();

function init() {
  // assigned button colors
  buttons[0].style.backgroundColor = colors[0];
  buttons[1].style.backgroundColor = colors[1];
  buttons[2].style.backgroundColor = colors[2];
  buttons[3].style.backgroundColor = colors[3];

  // click event handlers for buttons
  buttons[0].onclick = function() {
    pressed(0);
  };
  buttons[1].onclick = function() {
    pressed(1);
  };
  buttons[2].onclick = function() {
    pressed(2);
  };
  buttons[3].onclick = function() {
    pressed(3);
  };
}

// current active button
let current = 0;

// start the engine
// pick the first active button in 1500ms, after that every 1000ms
// 1500 is a parameter for setTimeout
// 1000 is a parameter for activate next
let timer = setTimeout(pickNext, 1500, 1000);

// empty array
let arr = [];

// function to keep the engine going: pick a new button and set timer for the next pick
function pickNext(delay) {
  // pick next button
  let next = pickNew(current);

  arr.push(next);
  if (arr.length >= 10) {
    gameOver();
  }

  // button colors updated
  buttons[current].style.backgroundColor = colors[current];
  buttons[current].classList.remove("active");
  buttons[next].style.backgroundColor = buttons[next];
  buttons[next].classList.add("active");

  // change the active button
  current = next;

  // set timer to pick the next button
  timer = setTimeout(pickNext, delay, delay * 0.99);

  //disable same button activation consecutively
  function pickNew(previous) {
    let next = getRandomInt(0, 3);
    if (next != previous) {
      return next;
    } else {
      return pickNew(previous);
    }
  }
}

let score = 0;

// This function is called whenever a button is pressed
function pressed(i) {
  console.log("Pressed:", i);
  if (i === arr[0]) {
    arr.shift(arr[0]);
    score++;
    document.getElementById("score").innerHTML = score;
  } else {
    gameOver();
  }
}

function gameOver() {
  //stops timer
  clearTimeout(timer);
  for (let i = 0; i < 4; i++) {
    // sets all buttons to red
    buttons[i].style.backgroundColor = "red";
    // click event handlers disabled
    buttons[i].onclick = null;

    //Show score
    // Set the overlay-element visible and update the gameover-element
    document.getElementById("overlay").style.visibility = "visible";
    if (score <= 100) {
      document.getElementById("gameover").innerHTML =
        "Game Over! <br>Your Score: </br>" + score;
    } else {
      document.getElementById("gameover").innerHTML =
        "Congratulations! <br>Your Score: </br>" + score;
    }
  }
}

// generate random integer within range min - max
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
